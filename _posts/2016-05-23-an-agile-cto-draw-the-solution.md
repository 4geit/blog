---
layout:     post
title:      Draw the solution
subtitle:   An Agile CTO
date:       2016-05-23 21:28:00
tags:       [agile-methodologies, software-project-management, agile-project-management]
author:     caner
header-img: "img/drawing.jpg"
---

You already learned in [my previous](/an-agile-cto-understanding-the-big-picture-of-a-new-product) post how to approach a new project with your product owner such as asking a set of questions and gathering information that will be crucial for the software elaboration. For those that missed the context of these series of posts, you can start looking at the initial post: [An Agile CTO Checklist](/an-agile-cto-checklist).

What I missed to detail are the tools that could be interesting to use in order to draw the diagrams and workflow (basically the Drafting solutions step). There are plenty of desktop, web and even mobile applications that allow to draw nice diagrams. What I am mostly using is the drawing tool of the google apps suites. It's surely very basic but do the work very efficiently. You don't need much features, a very basic drawing tool may be enough.

![](/img/google-drawing.png)

With this tool you can already draw squares, circles and arrows and that's all what we are going to need.

I know that at this stage it is very tempting to go on Internet and look for every kind of diagrams that are known in the literature for software architecture sketching, eventually ending up with UML, and try to draw every known single diagram. Wrong.

Thinking Agile, you should only draw what is important so far following a just-enough and iterative mindset. Pick a diagram (e.g. components or workflow diagram), not only the one that you are more comfortable with but one that could describe the best way your solution with data you gathered so far. Don't pay attention about the other diagrams for now and start drawing your diagram. Once you finished you can already add it to your folder among the other materials of the project and update it later as you go by getting more information from your product owner about the project requirements.

![](/img/workflow-example.png)

In my next post, I will explain how to create stories following the Agile terminology and introduce a very nice tool that will help you manage your project and collaborate with your team.
