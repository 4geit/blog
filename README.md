# Forge'it Blog

You can visit the blog of Forge'it from [blog.4ge.it](https://blog.4ge.it).

## Contributing

Please consider reading the [contributing guide](CONTRIBUTING.md) if you want to contribute to the project or create new content.

## Content Generator

The Team Blog uses the Jekyll Static Code Generator and is currently based on the Clean Blog theme by [Start Bootstrap](http://startbootstrap.com/).

If you have Jekyll installed, simply run `jekyll serve` in your command line and preview the build in your browser. You can use `jekyll serve --watch` to watch for changes in the source files as well.

A Grunt environment is also included. There are a number of tasks it performs like minification of the JavaScript, compiling of the LESS files, adding banners to keep the Apache 2.0 license intact, and watching for changes. Run the grunt default task by entering `grunt` into your command line which will build the files. You can use `grunt watch` if you are working on the JavaScript or the LESS.

You can run `jekyll serve --watch` and `grunt watch` at the same time to watch for changes and then build them all at once.
