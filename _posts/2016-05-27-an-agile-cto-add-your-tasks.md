---
layout:     post
title:      Add your tasks
subtitle:   An Agile CTO
date:       2016-05-27 09:58:00
tags:       [agile-project-management, agile-methodologies, software-project-management]
author:     caner
header-img: "img/task-circles.png"
---

Back to the Agile CTO posts series, I presented in [my previous post](/an-agile-cto-prepare-your-first-iteration) how to prepare your first iteration/sprint based on the agile project management tool called [Taiga](https://taiga.io/). You got a quick introduction to the Sprint Taskboard interface. In this post I will give you more details about how to create tasks, assign a user, close a story and understand the many tools that are available in this wonderful taskboard.

![](/img/task-circles-white.png)

There is not a single way of splitting up a feature into a number of tasks but what I am most of the time following is this hierarchy. In the highest level you can define each of your software feature or requirement as an epic (e.g. Contact Manager), in the middle end, define the stories related with this epic (e.g. As a logged user I want to sync my phone contact list with the app contact list so I can use them…) and in the last level, define the tasks related with each story. We are going to approach this last level, creating the tasks of each single story.

![](/img/taiga-taskboard.png)

From the Taskboard, one can see a plus button next to each story (each row), that allows to create a new task related with this very story. But as we saw previously you can also add multiple tasks at once using the bulk insertion feature.

![](/img/taiga-new-bulk-insert-tasks.png)

Every time you will add a new task, it will self-assign the "New" status and appear in the "New" column that is the task has been newly added and has not been started yet. But there are also other statuses such as:

* **In progress**, to define that the task work is currently in progress,

* **Ready for test**, to signal that the team finished to work on this task and now needs a deep testing stage before closing it,

* **Closed**, to define the task as closed means no more workload has to be given and the task has been fully tested as part of its related story.

* **Needs info**, to clearly state that information are missing in other to work on the task, so it is somehow blocked until someone can provide more details.

![](/img/taiga-taskboard-column.png)

You can move a task from one status to another anytime you want just by drag&drop the corresponding task from one column to another.

![](/img/taiga-assign-task-to.png)

Another interesting feature you can find out on the taskboard is the ability to assign a team member to a specific task. Very useful if a group of members are working together on the story but still want to split the tasks and assign them to each member. You can assign a member to the task by just clicking to the "Not assigned" link and a dialog will pop up inviting you to search for a user or select a user among a suggestion list.

![](/img/taiga-task-assigned.png)

And once you select the user you will see the picture of the selected user next to this task. You can actually also assign a team member to a whole story whole the same process in the story property.

![](/img/taiga-taskboard-top-panel.png)

You may also notice the black colored top panel with different numbers appear on it plus a progress bar. It actually allows you to have a quick summary of the state of your iteration. Therefore these numbers mean:

* **0%**, the iteration progression is at the beginning,

* **3.5 total points**, is the points you and your team assigned to the stories that are part of the iteration,

* **3 open tasks**, is the number of tasks that are still ongoing,

* **0 closed tasks**, is the number of tasks that have been already realized, tested and closed,

* **0 iocaine doses**, is the quantity of issues that have been defined for every available tasks in this iteration. Something I am planning to detail in another post.

![](/img/taiga-task-closed.png)

When you are closing a task, you will notice an update in the top panel.

![](/img/taiga-taskboard-top-panel-one-closed.png)

Basically the "closed tasks" number will be updated to 1 instead of its former number 0, and the "open tasks" number, 2 instead of 3. So you actually know at any moments the current remaining tasks left that still have to be closed.

![](/img/taiga-taskboard-many-closed.png)

Similarly, when every tasks of a story have been closed, the "completed points" number is also updated in the top panel based on the amount of points that have been assigned to this story. You may also notice that the progress bar and the percent number have also both changed based on the amount of points that have been completed so far.

![](/img/taiga-taskboard-unassigned-tasks.png)

At anytime during a live iteration, you can still add unexpected tasks thanks to the last row available in the taskboard.

![](/img/taiga-taskboard-chart.png)

Finally the Sprint Taskboard comes along with a very intuitive stats chart available by clicking on the stats button on the right of top panel. It shows the completed points for each single day that are part of the iteration (e.g. from May 25 to June 07) based on two curves:

* the dark green colored curve shows the actual points completion and,

* the light green curve, the conceptual approach in order to complete every stories of the iteration.

This tool can definitely help you prevent breakups during a live sprint. That's why it is very important to keep an eye on this chart and make sure the stories can still comply with the iteration deadline. if it does not you can adjust the stories and tasks accordingly.

In my next post, I will introduce the concept of scenarios in order to remove any risk of misunderstanding the requirements between the CTO, the team and the product owner. I will also present a new programmatic way of coding the scenarios, so every time new updates are committed to the project, a stack of tests is required and tested.
