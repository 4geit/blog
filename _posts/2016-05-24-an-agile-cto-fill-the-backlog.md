---
layout:     post
title:      Fill the backlog
subtitle:   An Agile CTO
date:       2016-05-24 21:32:00
tags:       [agile-methodologies, agile-project-management, software-project-management]
author:     caner
header-img: "img/backlog.jpg"
---

As an Agile CTO, you've just been involved in a new challenging product. You are already following a [checklist](/an-agile-cto-checklist) of what have to be done and understand the [big picture](/an-agile-cto-understanding-the-big-picture-of-a-new-product) of the product.

What's next? Some agile teams are used to use a simple whiteboard to keep track of their work progress, using notes for every single story/feature. And to have used this method many times I can tell that is very efficient but unfortunately very inefficient for distributed teams. But at the age of Internet we know we can do better. I already tried plenty of project management tools consistent with Agile methodologies or completely designed with Agile sight. Some are proprietary, others open sourced from Jira to Taiga through Pivotal, IceScrum, ScrumDo and Redmine.

![](/img/taiga-logo.png)
![](/img/jira-logo.png)

The most powerful tools that caught my attention by far are Taiga and Jira. The reason is they both efficiently integrate the most important features an Agile tool must definitely have. Jira, however can be considered as the new standard for startups and big companies though it is a proprietary solution and therefore has its limitations. It may even be required by your company to use it since it becomes more and more popular.

![](/img/jira-backlog.png)

Taiga covers every needed Agile features and is open-sourced but is also available as a service through https://taiga.io where you can subscribe a plan. Therefore you can install it in your own server or even customized the code for your own logic integrations. Although both offer a very complete API to connect with third party services such as Slack for being notified each time an event occurs.

![](/img/taiga-backlog.png)

Let's get a feel of using one of the tool, I have chosen to present here Taiga but you can obviously do the same with Jira. I will assume you are using their [free plan](https://taiga.io/) and we are using the Scrum method which is not very different from Lean. Once signed in and decided to create a new project, you will be invited to choose between two different templates.

![](/img/taiga-create-project.png)

Since you want to create a Scrum based project, you will select the Scrum template and can fill the asked information according to your project details. Once the project created you will be redirected to an empty backlog interface. From this point, things start to become interesting.

![](/img/taiga-team.png)

You can actually add as many collaborators as you want in your Taiga project including your product owner, CEO, investors, core team, … Taiga provides a way to customize permissions for each role/collaborator but also to create new roles.

You are then ready to work with your product owner, and fill the backlog. Go back to the backlog interface, and start adding new stories following the Agile User Story format so business users or customers can understand them. Make sure they also fit with the [INVEST characteristics](https://en.wikipedia.org/wiki/INVEST_(mnemonic)) for a good quality user story.

![](/img/taiga-new-user-story.png)

Create a new user story from the backlog, and fill the fields that are required. You have also a bulk adding tool that ease the process of inserting multiple user stories at once.

![](/img/taiga-bulk-insert-story.png)

Once added the backlog will look like the screenshot below. Taiga comes with other very useful features for Agile project management such as defining the points per story, letting every single collaborators vote the most important stories or also define a status about the story progress. For now let's focus on the prioritizing feature, you can reorder as many times as you want the stories within the backlog just by dragging & dropping the items in the list. The top of the backlog is considered as the high-priority items and the bottom, the lowest ones.

![](/img/taiga-backlog-stories.png)

Once you've prepared every stories with your product owner then you are ready to prepare your first iteration. I will explain how to create a new iteration/sprint in my next post.
