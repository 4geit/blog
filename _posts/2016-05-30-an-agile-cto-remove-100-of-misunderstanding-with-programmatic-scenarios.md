---
layout:     post
title:      Remove 100% of Misunderstanding with Programmatic Scenarios
subtitle:   An Agile CTO
date:       2016-05-30 10:18:00
tags:       [bdd, agile-methodologies, angularjs]
author:     caner
header-img: "img/diagram-drawing.png"
---

Context: This post is part of the "An Agile CTO" posts series that I started a few days ago. You can take a look at the [previous one](/an-agile-cto-add-your-tasks) which is about how to create tasks using an efficient agile project management tool.

In this post I will introduce a programmatic BDD (Behaviour-Driven Development) way of reducing seriously the communication gaps the Product Owner, CTO and team members may dramatically have without it. For this purpose, you should be familiar with the following techs stack and a few other tools that I will mention in the post but will not describe.

* [AngularJS](https://angularjs.org/): Front-End Javascript Framework for MVW web apps

* [Yadda](https://www.npmjs.com/package/yadda): Generic BDD test framework you can pair easily with any other JS based test framework

* [Mocha](https://mochajs.org/): A popular and very efficient unit test framework

* [Protractor](http://www.protractortest.org/): End-to-End test framework for AngularJS based apps

* [PhantomJS](http://phantomjs.org/): Headless Webkit Javascript API

You can actually use BDD with other technologies, plenty of BDD frameworks exist in differents programming languages, but I will introduce this method using these techs as I am more experiences with them.

The Behaviour-Driven Development technique is not a tool but instead can be considered as a whole paradigm-shift in software development methodologies. The idea is to give an opportunity for the Product Owner to describe the whole user scenarios in a human readable language called [Gherkin](https://github.com/cucumber/cucumber/wiki/Gherkin) and those side-by-side with the whole team that will actually elaborate the software solution. The product owner does not have to be a tech engineer, that's what the Gherkin language offers. (S)he can very easily describe any kind of scenarios without dealing how that behaviour is implemented.

Back to my last posts about the Agile User Story, a User Story actually describe a feature of software while the Scenario is a lower level to the User Story that enumerates a list of user scenarios that actually describe how the user can behave with this feature.

![](/img/agile-layers.png)

Let's consider that you are about to elaborate a web app for your product owner solution and the techs you are going to use are AngularJS and co. The good thing with AngularJS is that it comes along with a very efficient end-to-end test framework called Protractor where you can actually simulate user behavior on the app. Protractor itself is using a headless webkit API called PhantomJS in order to execute web browser actions on top of it. I recommend you work with your team to set up a front-end development environment using the end-to-end test framework Protractor.

There are also two others tools that have to be implemented in your development environment named Yadda and Mocha though Yadda also works with other test framework. Mocha is prefered here to Jasmine because of its minimalism. You can find the [user guide here](https://acuminous.gitbooks.io/yadda-user-guide/content/en/) to help you and your team to implement Yadda.

Once you finished to prepare your development environment, you can invite your product owner to write the feature files. Basically you create a "features" folder within your development environment that you share with your product owner thanks to a git repository or any other version controller and create one feature file per User Story. The product owner with your help can fill these feature files following the examples of implementation Yadda provides [here](https://github.com/acuminous/yadda/tree/master/examples). Here is also an example of how a feature file looks like:

Once these feature files have been filled, you can start working with your team in order to code the behavior of these scenarios. For this purpose, you will need to learn how to implement Steps with Yadda. Another nice feature Yadda allows is to create generic steps and scenarios so you can reuse them as a template, very useful when a very similar user scenario apply in different features.

Finally based on tools such as Gulp/Grunt, Wiredep, Connect-livereload and CI (Continuous Integration), it is possible to automate these scenarios tests so you don't need to launch the tests yourself or your team anytime a change occurred in the software source code.

The two last issues will be handled more in details in my next post.
