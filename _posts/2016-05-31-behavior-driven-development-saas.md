---
layout:     post
title:      Behavior-Driven Development SaaS
subtitle:   BDD Programmatic Scenarios
date:       2016-05-31 10:10:00
tags:       [agile-methodologies, software-project-management, bdd]
author:     caner
header-img: "img/agile-vs-bdd-tools.png"
---

Let's build something amazing, something that is sadly missing in the software development environments and that may make the difference and help decision makers to boost their software elaboration process. I thought a few months ago about creating an upper-level to the Agile Project Management tools such as Jira, Taiga or Pivotal that deal with the BDD programmatic scenarios. The current solutions that exist are not very easy to setup, you need to have lot of skills actually, not to write your scenarios, lot of progress have been done this way (cf. Gherkin), but to actually implement the scenarios behavior.

![](/img/agile-vs-bdd-tools-white.png)

More I am thinking about this and more I realize how complex it is. The good thing is that anyone that want to work on this kind of solution doesn't need to redo the wheel and create a complete new management tools from scratch. Indeed one can benefits from the already existing Jira or Taiga or any other Agile tools that provide a very complete API the new tool can actually connect each single User Story to Scenarios. Needless to say that this new tool can be designed to be consistent with non-Agile public tools such as the popular Github, Gitlab or Gogs that also provide very complete APIs, some hacks need to be done here though, isn't it?

![](/img/bdd-saas.png)

The tool should follow preferably an Open Source business model such as Growth Hacking/Freemium/SaaS models so it can allow anyone to install the solution on their own server, eventually customize it or benefits from a online free or more complete paid plan. The tool that I thought about should target generic but also specific software developments means it should help product owner, without any skills in programming. The later can define scenarios and implement its behaviors without needing to code anything but using pre-defined basic-blocks (called steps) that are specific for every types of software and some blocks can even be generic for all types of softwares.

![](/img/bdd-steps.png)

Let's dig deeper with an example. You are a product owner and you already have a general idea of how your mobile app should look like. Thanks to an Agile project management tool, you already enumerated the whole user stories you identified so far. Basically, the next step consists to hire a technical team, prioritize the stories together, prepare the first iteration and start the development process. Although the Agile methodologies help to involve more often the product owner to the product elaboration process, it still may face communication gaps. The product owner has a good idea of the software solution though (s)he may have issues to communicate efficiently with its tech team and those can obviously lead to misunderstandings and software releases that don't match the product owner initial requirements.

The BDD SaaS is not intended to solve every communications issues though we can improve significantly the level of details and in the same times reduce the risks of misunderstanding. The tool will allow the product owner, her/him-self, to define for each single user story, the relevant user scenarios and therefore have an exact description of the user behavior with a specific feature of the software. Back to your mobile app product, as a product owner, you will be able to describe every scenarios of your app without the needs of any technical skills following a business-readable language (cf. Gherkin) and once ready your team will only have to implement the scenarios and user behaviors you already defined thanks to the present tool.

![](/img/agile-vs-bdd-workflow.png)

Therefore you can get a better control on the overall software development workflow. Instead of having control in the Stories elaboration steps, the BDD SaaS can help you write user scenarios and define user behaviors before reaching the development stage. Another benefit could be inevitably the development cost, since the project preparation can be made upstream therefore it will not require specific skill to work at this stage.

All those things sound exciting but can we really implement such a tool. One of the solution I mentioned earlier was to use existing Agile project management tool such as JIRA or Taiga, connect their API with a new frontend that provide a Scenarios and Steps manager and in the same time provide a complete API as well, so developers can easily connect their development environment with this new BDD tool. Technically speaking tools such as Yadda, Protractor, Mocha or Karma (I introduced some of them in [my previous post](/an-agile-cto-remove-100-of-misunderstanding-with-programmatic-scenarios)) can be used to power the development environment. But instead of having to code the scenarios, a bridge that connects to the BDD SaaS API can fetch the product scenario remotely (ones that have been earlier written by the product owner) and allows the development team to instantaneously test every scenarios each time the software source code has been updated, so the team exactly know when the story has been actually implemented.

I believe such a tool can also shift our software development approach. Some product owner based on their budget will be probably interested to follow a just-in-time recruiting process where the whole software specifications including stories and scenarios have been already defined and will only require a few developers to work on these stories. The developers will be therefore better guided since they can comply to the scenarios and user behaviors provided by the product owner.

What do you think about this tool? Is it something that can boost your business? Do you know someone, a company or angels that could be interested to invest and help elaborate such a tool? You can also share the idea with people that you think can be interested with.
