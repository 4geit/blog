---
layout:     post
title:      Understanding the Big Picture of a New Product
subtitle:   An Agile CTO
date:       2016-05-22 20:48:00
tags:       [agile-methodologies, software-project-management, project-planning]
author:     caner
header-img: "img/understanding.jpg"
---

In [my previous post](/an-agile-cto-checklist) I listed a 6 steps checklist every Agile CTO in software development should consider. Let's detail in this post the following first step:

1. Acknowledge the project, set up and needs (elevator pitch, NOT list, criteria, neighbors, business models, ROI, budget, team roles).

It is very important to get the product owner as available as possible during the whole process of drafting and gathering all these information. Considering as well your work set up if it is on-site or remote work, use the good tools to communicate. Some can argue that asynchronous communication can be a good way. It will actually but not at this step. It is very important to create a conversational environment where the product owner can leverage and ask questions at any moment.

So don't be modest with tools such as phone call, skype/or any kind of video conf-based meeting and face-to-face meeting.

Keep in mind there will always be more to do than time and money will allow. It is important to remind this point to your product owner. It may also feel obvious but it is actually impossible to gather all the requirements subject to change at the beginning of a project.

Here are a few questions that will guide you to collect all the preliminary information I called the "big picture". Ask them to your product owner.

* **What and Why**: A quick reminder to understand why the product owner is here, who its customers are, and why (s)he decided to do this project in the first place.

* The well-known **Elevator Pitch**: Build a pitch following this format: For [target customer] who [statement of need or opportunity] the [product name] is a [product category] that [key benefit, compelling reason to buy]. Unlike [primary competitive alternative] our product [statement of primary differentiation].

* **NOT list**: Turning the project upside-down is also important. Build a list of items that will clearly state what is in and out of the scope for this project.

* **Neighbors**: With the help of the product owner, brainstorm everyone you think you are going to need to interact with before this project can go live. Let's say the project has to outsource some parts of its business logic process, they have to be listed here.

* **Drafting solutions**: You should start at this point drafting some solutions using tools such as diagrams (architectural diagrams, …), playing what-if scenarios or trying to get a feel of how big and complex this thing is. It is very important to draft all these with the help of the product owner, and getting all these materials written enable to get a read on what you are going up against technically and making sure everyone is cool with how you are going to build this thing. You can draw as many diagrams as needed such as components, workflow, class or proof of concept workflow diagrams.

* **Risks**: Understand the risks of the project and split them into two categories, those that are worth sweating and those that aren't. You can list here the challenges you and the team might face when you're delivering and what you can do to prevent them from ever seeing the light of day. It is obviously an ongoing list that you can update later with your team.

* **Rough timeline**: Try to figure out whether you have a one-, three-, or six-month project on your hands. Keep in mind this is almost just a guessing and you will provide a better estimation when the requirements and stories will be drafted later on.

* **Trade-off criterias**: Ask the product owner to rank its criterias in order of relative importance. Two criteria cannot have the same level of importance. As an example of criterias you can suggest: Quality, Budget, Time, Scope, … These criterias are very important for you and your team to lead the elaboration of the product accordingly.

* **Team roles**: Create a list of roles and their according competencies and expectations that will be needed to elaborate the project. One worker can eventually have several roles. Here are some example of roles you can use such as UX designer, Project Manager, Frontend Developer, Analyst, Dev-Op.

* **Decisional and core team**: Draw a diagram to clarify how the decisional team and core team are constituted. Define their names and their role in the project (Customer, Stakeholder, Core team, …)

* **Cost estimation**: At this step, give a rough budget needed to realize the project based on a rough calculation: Number of team members X duration of the project X hourly rates. Again this estimation is not accurate yet.

I will detail in my next post how to analyse the project requirements (diagrams, workflow, components, use cases) and will give you examples of tools you can use.
