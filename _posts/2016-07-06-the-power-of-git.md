---
layout:     post
title:      The Power of Git
subtitle:   In our daily tasks
date:       "2016-07-06 00:26:00"
tags:       [git,bookmyname,domain]
author:     caner
header-img: "img/git.png"
---

Lot of things have be made so far and I didn't find a good moment to write for almost a month. Good news I have lot of things to write about now. I prepared this post so I can actually set a checklist of the topics I am willing to write about next time. I started studying the [Gitlab working process](https://about.gitlab.com/2015/04/08/the-remote-manifesto/) and the inherent Open-Source workflow (see also [Remote only manifesto](https://www.remoteonly.org/)). They keep their work tracked as much as possible and to do so they intensively use Git repositories and other similar tools.

## Track everything with Git flow

By using Git repositories, it enables to track every updates made to a project. Therefore collaborators can go backward in the history of commitment in the project so they can understand what have been made by their team mate without having to ask. Which is very important and a consistent point regarding the asynchronous workflow we are following at [Forge’it](https://4ge.it) and means keep information written down and as transparent as possible to avoid interruptions.

I found out that other tools of collaborating such as CMS backend CRM tool or project management tool even based on Agile process don't offer the same functionalities that what git and git interfaces such as Gitab can offer. A git repository can be public or private. Anybody that have read access to the git repository can consult the history of the project hosted on it but also suggest updates on it. To do so a contributor can fork the project, means duplicate the project, update the files the user wants to improve and publish them in its own duplicate version of the project. When the user is ready to send its modifications to the original project, the user send a Merge Request that is still yet to be approved by the core team of the original project.

That's actually a very powerful feature that git offers because by doing so it enables to any systems that are based on git to be open for anyone contributions considering the project source code public and available world-wide. But it also applies at a company scale, instead of keeping product data, software source code, or any kind of digital data in an isolated file, using git repositories empower the team mate by letting them suggest improvements on others project without having to ask a permission. You see the picture, it offers a new dimension in our standard approach of collaborating. (See also [Innersourcing](https://about.gitlab.com/2014/09/05/innersourcing-using-the-open-source-workflow-to-improve-collaboration-within-an-organization/)).

## Domains Registrar Management

At forgeit we were already used to use version source control system such as git intensively but only to maintain software projects source code. We never used it for anything else. That’s what I was asking to myself. Why not? Why don't we use the same approach for any of our workflow as much as we can and in the same time give a way to track others contribution without having to ask. That is how I started thinking about all the tools that we were many used to use at forgeit one of them was our domain names registrar named bookmyname.com.

We bought most of our domain names at bookmyname.com and since we are often updating our domains DNS zones in order to add or update a sub domain, it was always needed to connect to the bookmyname.com frontend to do the job. But we quickly ended up with a few issues:

* First not everyone in the team had access to the admin credentials to update the domains and has to ask all the time to the IT or ralated admin to do the task just because of lack of permission. That's a shame and very counter productive right?
* Another issue we found was that bookmyname.com has no way to track the updates applied to the domain names in a way we could go backward in an history of event and know the whole story of a domain name.

For the later issue, using Git came naturally, we started by creating a public repository where all the domain names owned by Forge'it were gathered in it and every time we wanted to update a domain zone we needed to update the corresponding zone file available in the repository first so we can track down the updates of everyone and every domains.

That was a good improvement but the initial issue was still there. Some people that needed to update a domain name but just couldn't by lack of permissions had to resolve them to ask an IT that had the power to do so and sometime it was taking to much time because the IT or admin didn't have enough time.

## Continuous Deployment

![Gitlab-CI Pipelines](/img/gitlab-ci-pipelines.png)

The solution came with a technique already known in software development lifecycle as Continuous Deployment which basically means that every time you made an update in a production ready software code, there should have an automated deployment process that deploy the new version of the software in production.

> **For instance a mobile app**. Every time an update is made in the source code, their is an automated workflow that build the app, eventually test it and upload the new version to the mobile marketplace (Android or iOS).

So how Continuous Deployment (CD) can be useful for us? It almost obvious. Every time an update occurs in the repository that stores our domains config zone files, a CD process should be triggered in order to deploy the new version of domain zone file to the registrar cf. bookmyname.com. That's what I did, I created a CD script in order to automate the deployment process and thanks to the Gitlab continuous integration feature, it made it possible to trigger this script every time an update occurs in the repository.

![CasperJS Bookmyname Script](/img/casperjs-bookmyname-script.png)

The registrar doesn't expose an API to access easily to their features so I had to write a navigation script thanks to [CasperJS](http://casperjs.org/) and [PhantomJS](http://phantomjs.org). And it worked! Now not only all the members of Forge'it but even people out of the team can suggest updates in the Forge'it domains repository. If it is a forgeit member he/she probably has the permission to commit its updates without requiring approval. If it is someone else that doesn't have the permission, he can still send a Merge Request with its updates and if it fits the request, it will eventually be approved by the core team.

The Git repository where we are hosting our company domains setup are public and available on a [Gitlab repository](https://gitlab.com/4geit/domains). Merge Requests with improvements [are welcomed](https://gitlab.com/4geit/domains/blob/master/CONTRIBUTING.md). Though we are supporting only one registrar for now, the code has been designed to welcome more registrars, so feel free to fork the project and add yours (see providers folder).

## Epilog

That’s pretty much a good example of how we saved time by empowering everyone to be able to update our domain names, people in the company that was not used to configure a domain name just had to read the related documentation and started to do it by themself. And in the same time it offers a higher level of transparency since anybody can track the modifications that applied over the domains lifetime.

I definitely wanted to reproduce the same experience with other tools we were used to use and collaborate with, such as the Forge'it website, the blogs but also more technical components such as the servers infrastructure and on and on.

So here are a checklist of posts and topics that I am willing to talk about next time I write:

* How can I update a domain name at Forge'it?
* What is a static content generator, how useful it is?
* How can I update the Forge'it website?
* How can I update the Forge'it blog and write a new article?
* How can I update the Forge'it servers infrastructure?
