---
layout:     post
title:      Checklist
subtitle:   An Agile CTO
date:       2016-05-21 20:43:00
tags:       [modeling, agile-testing, agile-methodologies]
author:     caner
header-img: "img/checklist.jpg"
---

Basically a product owner/CEO just hired you to get involved in its new idea of software product. I wanted to share a checklist of different steps and tasks I am used to take care of before starting a new project. Let me know if I missed some steps or if you see any mistakes over the post. Oh, I almost forgot, make sure the product owner is always available during the whole steps that will follow, if not let her/him know how important it is to reduce any risks of misunderstanding.

1. Acknowledge the project, set up and needs (elevator pitch, NOT list, criteria, neighbors, business models, ROI, budget, team roles)

2. Analyse the project requirements (diagrams, workflow, components, use cases, draw as much as drafts as you can)

3. Define stories (create stories and put them in a top priorities backlog, more urgent on top and the less in the bottom, play the agile points definition game)

4. Usually Agile-doers are going straight to the next iteration preparation meeting where they decide what are the next stories they have to work on and start actually working on the product development. Wrong! Nowadays we have access to plenty of tools in order to programmatically define scenarios and therefore simulate user behaviors and interactions with the product. Behavior-Driven-Development is one of the technique that should be seriously considered and therefore to get a guideline that will ensure to eliminate any risks of mistake or misunderstanding with the product owner requirements. For every stories you should define/code at least one or two scenarios to make sure every time a new update has been made by the team the whole scenarios tests still pass. Ensure that these processes are made automatically by a server/SaaS service.

5. Use the power of Model-oriented programming/boilerplate codes to reduce the risk of redoing the wheel. There are already a large community of developers that shares models/generators for scaffolding the initial part of your software. Don't be afraid to use them, it will reduce the risk of missing some components that are essential for your software solution.

6. Prepare your development infrastructure (version control, release, continuous integration tools, bug tracker, wiki, …)

Now that you prepared all these steps, you are ready to tackle the hiring process in order to find the best collaborators and work with them in the development process of the product.

I will approach in more details each bullet points as well as the hiring step and those that will follow with my next posts.
