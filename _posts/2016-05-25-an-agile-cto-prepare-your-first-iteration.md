---
layout:     post
title:      Prepare your first Iteration
subtitle:   An Agile CTO
date:       2016-05-25 08:52:00
tags:       [agile-project-management, agile-methodologies, software-project-management]
author:     caner
header-img: "img/planning.jpg"
---

Following the post [Fill the Backlog](/an-agile-cto-fill-the-backlog) and the Scrum methodology, you are now ready to prepare your first iteration. Remember you previously filled the project backlog and reorder the stories according to your product owner priorities. Let's create now your first iteration called Sprint in the Scrum terms. Using the previously introduced [Taiga](https://taiga.io/) Agile project management tool that comes along with a very intuitive user interface, you can create a new sprint just by clicking to the according link.

![](/img/taiga-empty-sprints.png)

Then you will see a new dialog that just popped up inviting you to fill more details about your new sprint. You can therefore fill the name of the sprint, something that other collaborators can see and identify as the official sprint name, it can also be the identity of your software release or version number. Taiga also allows to set a start and end date of your sprint basically following the Scrum principles, a sprint should be planned for two weeks and that's why Taiga will fill these fields for you, but in case they don't fit your sprint set up you can still overwrite them.

![](/img/taiga-new-sprint.png)

And finally just create your sprint. You will notice some changes in the right sprints panel. Now you can drag&drop some stories inside your sprint. There are also some interesting information the sprints panel express such as the starting and ending date of the sprints, the quantity of closed stories as well as the list of stories that are part of each sprint. You may also noticed the link Sprint Taskboard and allows you to switch to the Sprint Taskboard view so you can see the stories in details, I will explain this tool a bit later.

![](/img/taiga-one-sprint.png)

To make things easier you can tick several stories that you want to move to the sprint and then just drag&drop them to the drop area you saw above.

![](/img/taiga-select-multiple-stories.png)

The sprint backlog is therefore filled with the stories you just moved in.

![](/img/taiga-one-sprint-with-stories.png)

Let's move now to the Sprint Taskboard view by clicking to the according button. At this moment you will see a new board where every rows correspond to the stories you moved to your sprint and columns for the status of the tasks you should create for each of your stories.

![](/img/taiga-taskboard.png)

I will detail a bit more how to manage and assign the tasks from the Sprint Taskboard in my next post. I will also show you some interesting stats tools that can help you prevent any breakups during your running sprints.
